# Setsuni Patches

- [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2837030324)

### Features :
- [**Asari & the Reapers**](https://steamcommunity.com/sharedfiles/filedetails/?id=2651149728) : Adds "Asari Terminal" into their traders inventories
- [**Over-Wall Nutrient Paste Dispenser**](https://steamcommunity.com/sharedfiles/filedetails/?id=2586462158) : Fixes cost of dispenser to be the same as Vanilla
- [**Rimworld**](https://store.steampowered.com/app/294100) : Changes all default storystellers requirements of expected pawns to four maximum

### TODO :
- [C#] [**Asari & the Reapers**](https://steamcommunity.com/sharedfiles/filedetails/?id=2651149728) : Fix [[NL] Facial Animations](https://steamcommunity.com/sharedfiles/filedetails/?id=2581693737) not working
- [C#] [**Asari & the Reapers**](https://steamcommunity.com/sharedfiles/filedetails/?id=2651149728) : Fix Reapers ship initial landing test
- [C#] [**Rimworld - Ideology**](https://store.steampowered.com/app/1392840/RimWorld__Ideology) : Set minimum age of Biosculpter AgeReversalCycle to "HumanlikeTeenager" minimum age